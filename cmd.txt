Git global setup
git config --global user.name "Mihailo"
git config --global user.email "volodimirovichm@bk.ru"

Create a new repository
git clone https://gitlab.com/Melnichenko_Mih/artificial-intelligence-systems.git
cd artificial-intelligence-systems
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Push an existing folder
cd existing_folder
git init
git remote add origin https://gitlab.com/Melnichenko_Mih/artificial-intelligence-systems.git
git add .
git commit -m "Initial commit"
git push -u origin master

Push an existing Git repository
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/Melnichenko_Mih/artificial-intelligence-systems.git
git push -u origin --all
git push -u origin --tags